import React, { Component, Fragment } from "react";
import Navbar from "../components/Navbar";
import { postEvent } from "../actions";
import { connect } from "react-redux";
import history from "../history";
import ImageUpload from "../components/ImageUpload"

class AddEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: "",
      date: "",
      participant: "",
      note: "",
    };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const {
      title,
      location,
      date,
      participant,
      note,
      image
    } = this.state
    this.props.postEvent({
      title,
      location,
      date,
      participant,
      note,
      image
    });
    this.props.history.push("/")
  };

  render() {
    return (
      <Fragment>
        <Navbar />
        <section className="login-block" method="post">
          <div className="container">
            <div className="card-group">
            <div className="card">
              <div className="card-body">
              <i className="fa fa-plus mr-sm-2 "></i>Add Event{" "}
                <span className="sr-only">(current)</span>
                <form className="mt-2" onSubmit={this.handleSubmit}>
                  <div className="form-group row">
                    <div className="col">
                      <div class="col-sm-12">
                        <input
                          type="text"
                          name="title"
                          value={this.state.title}
                          onChange={this.handleChange}
                          className="form-control"
                          id="title"
                          placeholder="Title"
                          required
                        />
                      </div>
                    </div>
                    <div className="col">
                      <div class="col-sm-12">

                        <input type="text" name="location"
                          value={this.state.location}
                          onChange={this.handleChange}
                          className="form-control"
                          id="location"
                          placeholder="Location"
                          required
                        />
                      </div>
                    </div>


                  </div>
                  <div className="form-group row">
                    <div className="col">
                      <div class="col-sm-12">
                        <input type="text" name="participant"
                          value={this.state.participant}
                          onChange={this.handleChange}
                          className="form-control"
                          id="participant"
                          placeholder="Participant"
                          required
                        />
                      </div>
                    </div>
                    <div className="col">
                      <div class="col-sm-12">

                        <input type="date" name="date"
                          value={this.state.date}
                          onChange={this.handleChange}
                          className="form-control"
                          id="date"
                          required
                        />
                      </div>
                    </div>


                  </div>
                  <div className="form-group row">
                    <div className="col">

                      <div class="col-sm-12">
                        <textarea
                          className="form-control"
                          name="note"
                          value={this.state.note}
                          onChange={this.handleChange}
                          id="note"
                          rows="3"
                          placeholder="Note"
                          required
                        ></textarea>
                      </div>
                    </div>


                  </div>
                  <div className="form-group row">
                    <div className="col">

                      <div class="col-sm-12">

                        <ImageUpload getURL={(image) => this.setState({ image })} />
                        <br></br>
                        <button type="submit" className="btn btn-primary">
                          Submit
                    </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="card">
              <div className="card-body">
              <img width="100%" height="100%"  src="https://img.freepik.com/free-vector/lovely-hand-drawn-planning-schedule-concept_23-2147936165.jpg?size=338&ext=jpg" class="card-img-top" />
                
              </div>
            </div>
         
            </div>
            
          </div>
        </section>
        );
      </Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  postEvent: (data) => dispatch(postEvent(data)),
});

export default connect(null, mapDispatchToProps)(AddEvent);
